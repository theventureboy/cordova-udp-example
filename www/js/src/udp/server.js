// Constructor. Address and port should
// be where you want the server to run.
var Server = function(address, port) {
  this.address = address;
  this.port = port || 0; // Port zero will auto-assign
}

// Convienience helper for setting up a message received
// callback. Note that we have to stringify the data because
// it is an ArrayBuffer.
Server.prototype.onReceive = function(callback) {
  chrome.sockets.udp.onReceive.addListener(function(info) {
    callback(UDP.Buffer.stringify(info.data), info);
  });
}

// Start the server. Once started, the function will
// yield the actual address and port of the server.
Server.prototype.listen = function(callback) {
  UDP.createSocket(this.address, this.port, function(socket, udp) {
    udp.getInfo(socket.socketId, function(server) {
      callback(server.localAddress, server.localPort);
    });
  });
}

window.UDP.Server = Server;
