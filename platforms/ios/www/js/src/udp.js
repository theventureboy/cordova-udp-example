var UDP = {
  // Helper function to create a new socket
  createSocket: function(address, port, callback) {
    chrome.sockets.udp.create(function(socket) {
      chrome.sockets.udp.bind(socket.socketId, address, port, function() {
        callback(socket, chrome.sockets.udp);
      });
    });
  },

  // All messages must be an ArrayBuffer. This helper
  // converts strings to ArrayBuffers and vice-versa.
  Buffer: {
    fromString: function(string) {
      var buffer = new ArrayBuffer(string.length * 2);
      var bufferView = new Uint16Array(buffer);
      for (var i = 0, strLen = string.length; i < strLen; i++) {
        bufferView[i] = string.charCodeAt(i);
      }
      return buffer;
    },

    stringify: function(buffer) {
      var bufferView = new Uint16Array(buffer);
      return String.fromCharCode.apply(null, bufferView);
    }
  }
};

window.UDP = UDP;
