// Constructor. The address and port should point to a UDP server.
var Client = function(address, port) {
  this.address = address;
  this.port = port || 0; // Port zero will auto-assign
}

// Send a message to the server. The callback is called once
// the message is sent.
Client.prototype.send = function(message, callback) {
  var address = this.address,
      port    = this.port,
      buffer  = UDP.Buffer.fromString(message);

  // Note that we're passing 0 for the port of the socket.
  // That's because we want to start a new udp socket on a random port,
  // then send the message to the server's port.
  UDP.createSocket(address, 0, function(socket, udp) {
    udp.send(socket.socketId, buffer, address, port, function(info) {
      callback(info);
      udp.close(socket.socketId);
    });
  });
}

window.UDP.Client = Client;
